//Chris Hinson
//Chapter 6 Program 10

public class Player {
    private String name;
    private int score = 0;
    private int rollTotal=0;
    private int turns = 0;

    public Player(String name)
    {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getTurns() {
        return turns;
    }

    public int getRollTotal() {
        return rollTotal;
    }

    public void roll()
    {
        Die die = new Die(6);
        die.roll();
        rollTotal = rollTotal +die.getValue();
        die.roll();
        rollTotal = rollTotal +die.getValue();
        score = score + rollTotal;
        turns++;
    }


}
