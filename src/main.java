//Chris Hinson
//Chapter 6 Program 10

import java.util.Scanner;

public class main {
    public static void main(String args[])
    {
        Scanner k = new Scanner(System.in);
        Boolean playing = Boolean.TRUE;

        System.out.println("What is your name?");
        String name = k.next();

        Player user = new Player(name);
        Player cpu = new Player("Computer");

        while (playing)
        {
            System.out.println("Would you like to roll? [Y/n]");
            String userIn = k.next();
            if (userIn.equals("Y")||userIn.equals("y")||userIn.equals("N")||userIn.equals("n")) {

                if ((userIn.equals("Y") || userIn.equals("y")) && user.getScore() < 21) {
                    //Play game
                    cpu.roll();
                    user.roll();
                    if (cpu.getScore() > 21) {
                        System.out.println("The computer has busted with a total of " + cpu.getScore() +" , you win");
                        playing = Boolean.FALSE;
                    }
                    else
                    {
                        System.out.println("You rolled a " + user.getRollTotal());
                        System.out.println("Your total is " + user.getScore());
                    }
                } else {
                    if (user.getTurns() > 0) {
                        System.out.println("Ok, stopping play now");
                        if (cpu.getScore() > user.getScore()) {
                            System.out.println("The computer has won : " + cpu.getScore() + " to " + user.getScore());
                        } else if (user.getScore() > cpu.getScore()){
                            System.out.println("You have won! : " + user.getScore() + " to " + cpu.getScore());
                        }
                        else
                        {
                            System.out.println("You have tied : " + cpu.getScore() + " to " + user.getScore());
                        }
                        playing = Boolean.FALSE;
                    } else {
                        System.out.println("Sorry you didn't want to play, come back soon");
                        playing = Boolean.FALSE;
                    }
                }
            }
            else
            {
                System.out.println("Sorry, that's not a valid input, please try again");
            }
        }

    }
}
